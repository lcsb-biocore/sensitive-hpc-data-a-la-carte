export function AboutPage(props: {}): JSX.Element {
  return (
    <>
      <h1>About</h1>
      <p>Some nice text with acknowledgements.</p>
    </>
  );
}

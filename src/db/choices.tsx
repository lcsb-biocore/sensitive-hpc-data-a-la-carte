import React from "react";
import { createContext, useContext, useState } from "react";
import { PropsWithChildren } from "react";

export type UserChoices = Record<string, Record<string, any>>;
export type UserChoicesState = [UserChoices, (_: UserChoices) => void];

const defaultContext: UserChoicesState = [
  {},
  (x) => {
    throw new Error("default choices used");
  },
];
export const UserChoicesCtx = createContext(defaultContext);

export function useChoices() {
  return useContext(UserChoicesCtx);
}

export function ChoicesProvider(props: PropsWithChildren<{}>) {
  const [choices, setChoices] = useState({});

  return (
    <UserChoicesCtx.Provider value={[choices, setChoices]}>
      {props.children}
    </UserChoicesCtx.Provider>
  );
}

export function getChoice(
  ctx: UserChoicesState,
  group: string,
  id: string,
  def: any
): any {
  const choices = ctx[0];
  if (!(group in choices)) return def;
  if (!(id in choices[group])) return def;
  return choices[group][id];
}

export function getChoiceVal(
  ctx: UserChoicesState,
  group: string,
  id: string,
  fn: (x: number) => number,
  def: any
): any {
  const choices = ctx[0];
  if (!(group in choices)) return def;
  if (!(id in choices[group])) return def;
  return fn(choices[group][id]);
}

export function setChoice(
  ctx: UserChoicesState,
  group: string,
  id: string,
  val: any
) {
  const [choices, setChoices] = ctx;
  var res = choices;
  if (!(group in res)) res[group] = {};
  res[group][id] = val;
  setChoices({ ...res });
}

export function unsetChoice(
  ctx: UserChoicesState,
  group: string,
  id: string,
  val: any
) {
  const [choices, setChoices] = ctx;
  var res = choices;
  if (!(group in res)) return;
  if (!(id in res[group])) return;
  delete res[group][id];
  setChoices({ ...res });
}

import ReactMarkdown from "react-markdown";

export const Md = ReactMarkdown;

export function Text(props: { children: JSX.Element }): JSX.Element {
  return <div className="read-paragraph">{props.children}</div>;
}

export function MdText(props: { children: string }): JSX.Element {
  return (
    <div className="read-paragraph">
      <Md>{props.children}</Md>
    </div>
  );
}

import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";

import { ObjBadge } from "c/ObjBadge";

import { actors, threats, vulns, impacts, precs } from "db/contents";

function DbList<T extends { label: string }>(
  obj: string,
  collection: Record<string, T>
): JSX.Element {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>
            <ObjBadge obj={obj} />
          </th>
          <th>Identifier</th>
        </tr>
      </thead>
      <tbody>
        {Object.keys(collection).map((k: string) => (
          <tr key={k}>
            <td>
              <Link to={`/db/${obj}/${k}`}>{collection[k].label}</Link>
            </td>
            <td>
              <code>{k}</code>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

export function ActorList() {
  return DbList("actor", actors);
}

export function ImpactList() {
  return DbList("impact", impacts);
}

export function PrecList() {
  return DbList("prec", precs);
}

export function ThreatList() {
  return DbList("threat", threats);
}

export function VulnList() {
  return DbList("vuln", vulns);
}

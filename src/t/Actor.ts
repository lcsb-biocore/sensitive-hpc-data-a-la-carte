export type Actor = {
  label: string;
  description: string;
  estimation: string;
  estimate: number;
};

export type Actors = Record<string, Actor>;

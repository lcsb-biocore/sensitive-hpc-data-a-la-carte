import { Actors } from "t/Actor";
import { Impacts } from "t/Impact";
import { Precautions } from "t/Precaution";
import { Threats } from "t/Threat";
import { Vulns } from "t/Vuln";

import actor_data from "db/data/actors.json";
import impact_data from "db/data/impacts.json";
import prec_data from "db/data/precs.json";
import threat_data from "db/data/threats.json";
import vuln_data from "db/data/vulns.json";

export const actors: Actors = actor_data;
export const impacts: Impacts = impact_data;
export const precs: Precautions = prec_data;
export const threats: Threats = threat_data;
export const vulns: Vulns = vuln_data;

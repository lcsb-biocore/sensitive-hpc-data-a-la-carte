export type Precaution = {
  label: string;
  description: string;
  reduction: number;
};

export type Precautions = Record<string, Precaution>;

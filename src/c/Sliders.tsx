import { useState } from "react";

import Form from "react-bootstrap/Form";

import RangeSlider from "react-bootstrap-range-slider";
import "react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css";

import { useChoices, getChoice, setChoice } from "db/choices";

import si from "si-prefix";
const moneyScale = new si.Scale({
  above: { 3: "k", 6: "Mn", 9: "Bn", 12: "Tn" },
  below: {},
});
const moneyUnit = new si.Unit(moneyScale, "🪙");

type CheckboxParams = { group: string; id: string };
type SliderParams = { group: string; id: string; def: number };

export function valMoney(amount: number): number {
  return valAmount(amount);
}

export function fmtMoney(amount: number): string {
  return fmtMoneyVal(valMoney(amount));
}

export function fmtMoneyVal(val: number): string {
  return moneyUnit.format(Number(val.toPrecision(3)), " ");
}

export function ivalMoney(val: number): number {
  return ivalAmount(val);
}

export function MoneySlider({ group, id, def }: SliderParams): JSX.Element {
  const ctx = useChoices();
  const [val, setVal] = useState(getChoice(ctx, group, id, ivalMoney(def)));
  return (
    <RangeSlider
      min={-1}
      max={100}
      value={val}
      tooltip="on"
      tooltipPlacement="bottom"
      onChange={(event) => setVal(parseInt(event.target.value))}
      onAfterChange={(event) => setChoice(ctx, group, id, event.target.value)}
      tooltipLabel={fmtMoney}
    />
  );
}

export function valAmount(amount: number): number {
  if (amount >= 0) {
    const scale = Math.floor(amount / 9);
    const num = amount % 9;
    const res = (1 + num) * Math.pow(10, scale);
    return res;
  } else {
    return 0;
  }
}

export function ivalAmount(val: number): number {
  if (0 >= val) return -1;
  const base = Math.trunc(Math.log(val) / Math.log(10));
  return 9 * base + Math.trunc(val / Math.pow(10, base)) - 1;
}

export function fmtAmount(amount: number): string {
  return fmtAmountVal(valAmount(amount));
}

export function fmtAmountVal(val: number): string {
  return `${val}`;
}

export function AmountSlider({ group, id, def }: SliderParams): JSX.Element {
  const ctx = useChoices();
  const [val, setVal] = useState(getChoice(ctx, group, id, ivalAmount(def)));
  return (
    <RangeSlider
      min={-1}
      max={54}
      value={val}
      tooltip="on"
      tooltipPlacement="bottom"
      onChange={(event) => setVal(parseInt(event.target.value))}
      onAfterChange={(event) => setChoice(ctx, group, id, event.target.value)}
      tooltipLabel={fmtAmount}
    />
  );
}

export function valPeriod(amount: number): number {
  return Math.pow(2, -amount / 10);
}

export function ivalPeriod(val: number): number {
  return Math.round((-10 * Math.log(val)) / Math.log(2));
}

export function fmtPeriod(amount: number): string {
  return fmtPeriodVal(valPeriod(amount));
}

export function fmtPeriodVal(seconds: number): string {
  if (seconds >= valPeriod(-310)) return "likely never";
  if (seconds >= 365 * 86400)
    return `every ${Math.round(seconds / 365 / 86400)} years`;
  if (seconds >= 30 * 86400)
    return `every ${Math.round(seconds / 30 / 86400)} months`;
  if (seconds >= 7 * 86400)
    return `every ${Math.round(seconds / 7 / 86400)} weeks`;
  if (seconds >= 86400) return `every ${Math.round(seconds / 86400)} days`;
  if (seconds >= 3600) return `every ${Math.round(seconds / 3600)} hours`;
  if (seconds >= 60) return `every ${Math.round(seconds / 60)} minutes`;
  return `every ${Math.round(seconds)} seconds`;
}

export function PeriodSlider({ group, id, def }: SliderParams): JSX.Element {
  const ctx = useChoices();
  const [val, setVal] = useState(getChoice(ctx, group, id, ivalPeriod(def)));
  return (
    <RangeSlider
      min={-310}
      max={0}
      value={val}
      tooltip="on"
      tooltipPlacement="bottom"
      onChange={(event) => setVal(parseInt(event.target.value))}
      onAfterChange={(event) => setChoice(ctx, group, id, event.target.value)}
      tooltipLabel={fmtPeriod}
    />
  );
}

export function valProb(amount: number): number {
  return Math.pow(10, amount / 10);
}

export function ivalProb(val: number): number {
  return Math.round((10 * Math.log(val)) / Math.log(10));
}

export function fmtProb(amount: number): string {
  return fmtProbVal(valProb(amount));
}

export function fmtProbVal(val: number): string {
  return `${(100 * val).toPrecision(3)}%`;
}

export function ProbSlider({ group, id, def }: SliderParams): JSX.Element {
  const ctx = useChoices();
  const [val, setVal] = useState(getChoice(ctx, group, id, ivalProb(def)));
  return (
    <RangeSlider
      min={-80}
      max={0}
      value={val}
      tooltip="on"
      tooltipPlacement="bottom"
      onChange={(event) => setVal(parseInt(event.target.value))}
      onAfterChange={(event) => setChoice(ctx, group, id, event.target.value)}
      tooltipLabel={fmtProb}
    />
  );
}

export function CheckBox({ group, id }: CheckboxParams): JSX.Element {
  const ctx = useChoices();
  const [st, setSt] = useState(getChoice(ctx, group, id, false));
  return (
    <Form>
      <Form.Check
        type="switch"
        label={
          st
            ? "This precaution has been applied."
            : "This precaution has not been applied."
        }
        checked={st}
        onChange={() => {
          setSt(!st);
          setChoice(ctx, group, id, !st);
        }}
      />
    </Form>
  );
}

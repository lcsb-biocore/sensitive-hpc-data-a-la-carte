export type Threat = {
  label: string;
  description: string;
  actor: string;
  estimation: string;
  estimate: number;
};

export type Threats = Record<string, Threat>;

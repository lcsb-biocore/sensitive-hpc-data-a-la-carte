import { Link } from "react-router-dom";

export function Db() {
  return (
    <>
      <h1>Knowledge base</h1>
      <p>
        All objects in the current database can be explored in complete
        listings:
      </p>
      <ul>
        <li>
          <Link to="/db/actors">actors</Link>
        </li>
        <li>
          <Link to="/db/impacts">impacts</Link>
        </li>
        <li>
          <Link to="/db/precs">precautions</Link>
        </li>
        <li>
          <Link to="/db/threats">threats</Link>
        </li>
        <li>
          <Link to="/db/vulns">vulnerabilities</Link>
        </li>
      </ul>
      <p>
        You may download the complete database contents in a machine-readable
        formats:
      </p>
      <ul>
        <li>JSON (TODO)</li>
        <li>TypeScript (actual source code data, TODO)</li>
      </ul>
    </>
  );
}

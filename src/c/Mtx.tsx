import { Table, Card, ListGroup } from "react-bootstrap";

import { ObjBadge, VulnPrecsBadge } from "c/ObjBadge";

import { actors, threats, vulns, impacts } from "db/contents";

function threatOrder(t1: string, t2: string): number {
  const a1 = threats[t1].actor;
  const a2 = threats[t2].actor;

  if (a1 < a2) return -1;
  if (a1 > a2) return 1;
  if (t1 < t2) return -1;
  if (t1 > t2) return 1;
  return 0;
}

type elems = Record<string, Record<string, string[]>>;

function makeElems(): elems {
  let x: elems = {};
  for (const v in vulns)
    for (const tix in vulns[v].threats)
      for (const iix in vulns[v].impacts) {
        const t = vulns[v].threats[tix];
        const i = vulns[v].impacts[iix];
        if (!(i in x)) x[i] = {};
        if (!(t in x[i])) x[i][t] = [];
        x[i][t].push(v);
      }
  return x;
}

const mtxcols: string[] = Object.keys(impacts);
const mtxrows: string[] = Object.keys(threats).sort(threatOrder);
const mtxelems: elems = makeElems();

function VulnCard(props: { v: string }): JSX.Element {
  const { v } = props;
  //TODO: this might be best showing the summary precaution fixing status (say
  //2 OK, 5 precautions still applicable), the expected monetary impact of the vulnerability, and
  //the amount that can still be fixed
  return (
    <Card className="mb-1">
      <Card.Header>
        <div>{vulns[v].label}</div>
        <ObjBadge obj="vuln" link={v} />
      </Card.Header>
      <ListGroup variant="flush">
        <ListGroup.Item>
          Mitigation: <VulnPrecsBadge id={v} />
        </ListGroup.Item>
      </ListGroup>
    </Card>
  );
}

function MtxElem(props: { t: string; i: string }): JSX.Element {
  const { t, i } = props;
  if (i in mtxelems && t in mtxelems[i])
    return (
      <>
        {mtxelems[i][t].map((v) => (
          <VulnCard key={v} v={v} />
        ))}
      </>
    );
  else return <></>;
}

function MtxRow(props: { t: string }): JSX.Element {
  const { t } = props;
  return (
    <tr key={t}>
      <td>
        <div>{actors[threats[t].actor].label}</div>
        <ObjBadge obj="actor" link={threats[t].actor} />
      </td>
      <td>
        <div>{threats[t].label}</div>
        <ObjBadge obj="threat" link={t} />
      </td>
      {mtxcols.map((i: string) => (
        <td key={i}>
          <MtxElem t={t} i={i} />
        </td>
      ))}
    </tr>
  );
}

export function Mtx() {
  return (
    <>
      <h1>Vulnerability check matrix</h1>
      <Table>
        <thead>
          <tr>
            <td></td>
            <td></td>
            {mtxcols.map((i: string) => (
              <th key={i}>
                <div>{impacts[i].label}</div>
                <ObjBadge obj="impact" link={i} />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {mtxrows.map((t: string) => (
            <MtxRow key={t} t={t} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

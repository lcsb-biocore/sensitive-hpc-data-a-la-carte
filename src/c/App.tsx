import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import "./App.scss";

import { LinkContainer } from "react-router-bootstrap";
import { Alert, Container, Navbar, Nav, Row, Col } from "react-bootstrap";
import { Map, BarChart, Grid, Database, Info } from "react-feather";

import { ChoicesProvider } from "db/choices";
import { Actor, Impact, Prec, Threat, Vuln } from "c/DbPage";
import {
  ActorList,
  ImpactList,
  PrecList,
  ThreatList,
  VulnList,
} from "c/DbList";
import { Db } from "c/Db";
import { Mtx } from "c/Mtx";

import { Results } from "c/Results";
import { IndexPage } from "c/static/IndexPage";
import { IntroPage } from "c/static/IntroPage";
import { AboutPage } from "c/static/AboutPage";

import permedcoeLogo from "logos/permedcoe.svg";
import uniluLogo from "logos/unilu.svg";
import lcsbLogo from "logos/lcsb.svg";

function App() {
  return (
    <div className="App">
      <HashRouter>
        <Navbar bg="dark" variant="dark">
          <Container>
            <LinkContainer to="/">
              <Navbar.Brand>Sensitive data in HPC à la carte</Navbar.Brand>
            </LinkContainer>
            <Nav className="header">
              <LinkContainer to="/intro">
                <Nav.Link>
                  <Map size="1em" /> Introduction
                </Nav.Link>
              </LinkContainer>
              <LinkContainer to="/matrix">
                <Nav.Link>
                  <Grid size="1em" /> Check matrix
                </Nav.Link>
              </LinkContainer>
              <LinkContainer to="/results">
                <Nav.Link>
                  <BarChart size="1em" /> My results
                </Nav.Link>
              </LinkContainer>
              <LinkContainer to="/db">
                <Nav.Link>
                  <Database size="1em" /> View database
                </Nav.Link>
              </LinkContainer>
              <LinkContainer to="/about">
                <Nav.Link>
                  <Info size="1em" /> About
                </Nav.Link>
              </LinkContainer>
            </Nav>
          </Container>
        </Navbar>
        <Container>
          <ChoicesProvider>
            <Row className="mt-5 body">
              <Col>
                <Routes>
                  <Route
                    path="*"
                    element={<Alert variant="danger">Page not found.</Alert>}
                  />
                  <Route index element={<IndexPage />} />
                  <Route path="intro" element={<IntroPage />} />
                  <Route path="matrix" element={<Mtx />} />
                  <Route path="results" element={<Results />} />
                  <Route path="db" element={<Db />} />
                  <Route path="about" element={<AboutPage />} />

                  <Route path="db/actors" element={<ActorList />} />
                  <Route path="db/impacts" element={<ImpactList />} />
                  <Route path="db/precs" element={<PrecList />} />
                  <Route path="db/threats" element={<ThreatList />} />
                  <Route path="db/vulns" element={<VulnList />} />
                  <Route path="db/actor/:id" element={<Actor />} />
                  <Route path="db/impact/:id" element={<Impact />} />
                  <Route path="db/prec/:id" element={<Prec />} />
                  <Route path="db/threat/:id" element={<Threat />} />
                  <Route path="db/vuln/:id" element={<Vuln />} />
                </Routes>
              </Col>
            </Row>
            <Row className="footer bg-light text-muted mt-5 p-5">
              <Col>
                Remember this application is useful for rough estimation and
                illustration purposes only. Validate the methodology formally
                before you draw conclusions about the security of your data.
              </Col>
              <Col>
                <img
                  className="footer-logo"
                  src={permedcoeLogo}
                  alt="PerMedCoE"
                />
                <img
                  className="footer-logo"
                  src={uniluLogo}
                  alt="University of Luxembourg"
                />
                <img className="footer-logo" src={lcsbLogo} alt="LCSB" />
              </Col>
            </Row>
          </ChoicesProvider>
        </Container>
      </HashRouter>
    </div>
  );
}

export default App;

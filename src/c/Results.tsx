import { Table } from "react-bootstrap";
import { ObjBadge } from "c/ObjBadge";

import { fmtPeriodVal, fmtMoneyVal, fmtProbVal } from "c/Sliders";

import { useChoices } from "db/choices";
import {
  estThreatTotalPeriods,
  estMitigatedVulnPeriods,
  estImpactPeriods,
  estImpactCosts,
  estTotalImpactPeriod,
  estTotalImpactCost,
} from "db/estimate";
import { impacts } from "db/contents";

export function Results() {
  const ch = useChoices();
  const ttps = estThreatTotalPeriods(ch);
  const mvps = estMitigatedVulnPeriods(ch, ttps);
  const ics = estImpactCosts(ch, mvps);
  const ips = estImpactPeriods(ch, mvps);
  const tc = estTotalImpactCost(ics);
  const tp = estTotalImpactPeriod(ips);

  return (
    <>
      <h1>Results and estimations</h1>
      <h5>Expected impact summary</h5>
      <Table>
        <thead>
          <tr>
            <th>Impact</th>
            <th>Time between events</th>
            <th>Amortized montly cost</th>
            <th>Amortized yearly cost</th>
            <th>Probability this month</th>
            <th>Probability this year</th>
          </tr>
        </thead>
        <tbody>
          <>
            {Object.keys(impacts).map((i) => (
              <tr key={i}>
                <td>
                  {impacts[i].label} <ObjBadge obj="impact" link={i} />
                </td>
                <td>{fmtPeriodVal(ips[i])}</td>
                <td>{fmtMoneyVal(30 * 86400 * ics[i])}</td>
                <td>{fmtMoneyVal(365 * 86400 * ics[i])}</td>
                <td>{fmtProbVal(1 - Math.exp((-30 * 86400) / ips[i]))}</td>
                <td>{fmtProbVal(1 - Math.exp((-365 * 86400) / ips[i]))}</td>
              </tr>
            ))}
            <tr>
              <th>Any impact</th>
              <td>{fmtPeriodVal(tp)}</td>
              <td>{fmtMoneyVal(30 * 86400 * tc)}</td>
              <td>{fmtMoneyVal(365 * 86400 * tc)}</td>
              <td>{fmtProbVal(1 - Math.exp((-30 * 86400) / tp))}</td>
              <td>{fmtProbVal(1 - Math.exp((-365 * 86400) / tp))}</td>
            </tr>
          </>
        </tbody>
      </Table>
      <h5>TODO: Top dangerous actors</h5>
      <h5>Top expected impacts</h5>
      <h5>Top adviseable precautions</h5>
      <h5>Top threats</h5>
      <h5>Top vulnerabilities</h5>
    </>
  );
}

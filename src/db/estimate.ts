import { getChoice, getChoiceVal, UserChoicesState } from "db/choices";

import { valMoney, valAmount, valPeriod, valProb } from "c/Sliders";

import { actors, threats, vulns, impacts, precs } from "db/contents";

type IdNumMap = Record<string, number>;

export function estActorCount(ctx: UserChoicesState, id: string): number {
  return getChoiceVal(ctx, "actor", id, valAmount, actors[id].estimate);
}

export function estThreatBasePeriod(ctx: UserChoicesState, id: string): number {
  return getChoiceVal(ctx, "threat", id, valPeriod, threats[id].estimate);
}

export function estThreatTotalPeriod(
  ctx: UserChoicesState,
  id: string
): number {
  return estThreatBasePeriod(ctx, id) / estActorCount(ctx, threats[id].actor);
}

export function estThreatTotalPeriods(ctx: UserChoicesState): IdNumMap {
  var res: IdNumMap = {};
  for (const id in threats) res[id] = estThreatTotalPeriod(ctx, id);
  return res;
}

export function getVulnPrecs(id: string): string[] {
  var done = new Set<string>();
  var todo = new Set<string>([id]);
  var pids = new Set<string>();
  while (todo.size > 0) {
    const cur = todo.entries().next().value[0];
    todo.delete(cur);
    if (cur in done) continue;
    done.add(cur);
    for (const pidx in vulns[cur].precs) pids.add(vulns[cur].precs[pidx]);
    const rs = vulns[cur].amplifies;
    if (rs)
      for (const vidx in rs) {
        const vid: string = rs[vidx];
        if (!(vid in done)) todo.add(vid);
      }
  }
  return Array.from(pids);
}

export function getVulnPrecsCount(
  ctx: UserChoicesState,
  id: string
): { applied: number; unapplied: number } {
  var applied = 0;
  var unapplied = 0;
  const vprecs = getVulnPrecs(id);
  for (const pidx in vprecs) {
    if (getChoice(ctx, "prec", vprecs[pidx], false)) applied = applied + 1;
    else unapplied = unapplied + 1;
  }
  return { applied, unapplied };
}

export function estVulnPeriod(
  ctx: UserChoicesState,
  id: string,
  threatTotalPeriods: IdNumMap
): number {
  var sum = 0;
  for (const tidx in vulns[id].threats) {
    const tid = vulns[id].threats[tidx];
    sum += 1 / threatTotalPeriods[tid];
  }
  return 1 / sum / getChoiceVal(ctx, "vuln", id, valProb, vulns[id].estimate);
}

export function estVulnPeriods(
  ctx: UserChoicesState,
  threatTotalPeriods: IdNumMap
): IdNumMap {
  var res: IdNumMap = {};
  const ttps = estThreatTotalPeriods(ctx);
  for (const id in vulns) res[id] = estVulnPeriod(ctx, id, ttps);
  return res;
}

export function estVulnMitigation(ctx: UserChoicesState, id: string): number {
  var prob = 1;
  const vprecs = getVulnPrecs(id);
  for (const pidx in vprecs) {
    const pid = vprecs[pidx];
    if (getChoice(ctx, "prec", pid, false)) prob *= precs[pid].reduction;
  }
  return prob;
}

export function estVulnMitigations(ctx: UserChoicesState): IdNumMap {
  var res: IdNumMap = {};
  for (const id in vulns) res[id] = estVulnMitigation(ctx, id);
  return res;
}

export function estVulnFullMitigation(
  ctx: UserChoicesState,
  id: string
): number {
  var prob = 1;
  const vprecs = getVulnPrecs(id);
  for (const pidx in vprecs) {
    const pid = vprecs[pidx];
    prob *= precs[pid].reduction;
  }
  return prob;
}

export function estVulnFullMitigations(ctx: UserChoicesState): IdNumMap {
  var res: IdNumMap = {};
  for (const id in vulns) res[id] = estVulnFullMitigation(ctx, id);
  return res;
}

export function estMitigatedVulnPeriod(
  ctx: UserChoicesState,
  id: string,
  threatTotalPeriods: IdNumMap
): number {
  return (
    estVulnPeriod(ctx, id, threatTotalPeriods) / estVulnMitigation(ctx, id)
  );
}

export function estMitigatedVulnPeriods(
  ctx: UserChoicesState,
  threatTotalPeriods: IdNumMap
): IdNumMap {
  var res: IdNumMap = {};
  for (const id in vulns)
    res[id] = estMitigatedVulnPeriod(ctx, id, threatTotalPeriods);
  return res;
}

export function estFullMitigatedVulnPeriod(
  ctx: UserChoicesState,
  id: string,
  threatTotalPeriods: IdNumMap
): number {
  return (
    estVulnPeriod(ctx, id, threatTotalPeriods) / estVulnFullMitigation(ctx, id)
  );
}

export function estFullMitigatedVulnPeriods(
  ctx: UserChoicesState,
  threatTotalPeriods: IdNumMap
): IdNumMap {
  var res: IdNumMap = {};
  for (const id in vulns)
    res[id] = estFullMitigatedVulnPeriod(ctx, id, threatTotalPeriods);
  return res;
}

export function estImpactPeriod(
  ctx: UserChoicesState,
  id: string,
  vulnPeriods: IdNumMap
): number {
  var sum = 0;
  for (const vid in vulns)
    if (vulns[vid].impacts.includes(id)) sum += 1 / vulnPeriods[vid];
  return 1 / sum;
}

export function estImpactPeriods(
  ctx: UserChoicesState,
  vulnPeriods: IdNumMap
): IdNumMap {
  var res: IdNumMap = {};
  for (const id in impacts) res[id] = estImpactPeriod(ctx, id, vulnPeriods);
  return res;
}

export function estTotalImpactPeriod(impactPeriods: IdNumMap): number {
  var sum = 0;
  for (const id in impacts) sum += 1 / impactPeriods[id];
  return 1 / sum;
}

export function estImpactCost(
  ctx: UserChoicesState,
  id: string,
  vulnPeriods: IdNumMap
): number {
  return (
    getChoiceVal(ctx, "impact", id, valMoney, impacts[id].estimate) /
    estImpactPeriod(ctx, id, vulnPeriods)
  );
}

export function estImpactCosts(
  ctx: UserChoicesState,
  vulnPeriods: IdNumMap
): IdNumMap {
  var res: IdNumMap = {};
  for (const id in impacts) res[id] = estImpactCost(ctx, id, vulnPeriods);
  return res;
}

export function estTotalImpactCost(impactCosts: IdNumMap): number {
  var sum = 0;
  for (const id in impacts) sum += impactCosts[id];
  return sum;
}

// TODO: "cost" of a single vulnerability, "value" of implementing a single precaution

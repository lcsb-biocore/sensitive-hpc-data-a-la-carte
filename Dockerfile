
FROM node AS builder
ADD . /src/hpcdata/
RUN cd /src/hpcdata/ && \
    yarnpkg install --prod && \
    yarnpkg build

FROM nginx:alpine-slim
COPY --from=builder /src/hpcdata/build /usr/share/nginx/html

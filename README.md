# Software development guidelines for handling sensitive data in HPC (à la carte)

The idea of this project is to make a small single-page javascript app that
organizes the tons of general advice on how to make HPC software more resilient
against losing, inadvertently publishing or otherwise endangering sensitive
data.

- main data types ("schema") for things are in `src/t`
- the database is in `src/db` (edit this if you want to add vulnerabilities);
  that is currently incomplete (going to be always incomplete by definition,
  but we can get pretty comprehensive)
- the app components for a small app viewer are in `src/c`, that is not working
  yet. In the end it should include a small stateful "checklist" app that
  allows you to assign various priorities to the threats and impacts, tick the
  vulnerabilities that you have avoided, and see what you should focus on next

## How to run this

This project was bootstrapped with
[Create React App](https://github.com/facebook/create-react-app),
and (to date) retains the base react scriptage.

You need `npm` or `yarn`, install with `apt-get install yarnpkg` or similar
command appropriate for your OS.

To install dependencies, run `yarnpkg install` (or `npm install`).

To start the development mode, run `yarnpkg start` (again the same with `npm`).
This will start the app in [http://localhost:3000](http://localhost:3000) and
you will be able to play with it in the browser.  The page will reload if you
make edits, and you will also see any lint errors in the console.

## Deployment

`yarnpkg build` creates a static website in the `build` directory that can be
deployed to a webserver (or github pages, etc.).

## How to customize the database

All data available in the app can be edited as JSON in `src/db/data`. After
modifying the database, simply re-deploy the app.

You should be download JSON from other running instances using the link in the
"database" tab.

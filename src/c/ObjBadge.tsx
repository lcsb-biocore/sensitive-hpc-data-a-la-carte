import { Badge } from "react-bootstrap";
import { Link } from "react-router-dom";

import { useChoices, getChoice } from "db/choices";
import { getVulnPrecsCount } from "db/estimate";

const badgeMeta: Record<string, { b: string; n: string }> = {
  actor: { b: "primary", n: "Actor" },
  impact: { b: "info", n: "Impact" },
  prec: { b: "success", n: "Precaution" },
  threat: { b: "danger", n: "Threat" },
  vuln: { b: "warning", n: "Vulnerability" },
};

export function objName(x: string): string {
  return x in badgeMeta ? badgeMeta[x].n : "?";
}

export function ObjBadge(props: { obj: string; link?: string }): JSX.Element {
  const n = objName(props.obj);
  const b = props.obj in badgeMeta ? badgeMeta[props.obj].b : "secondary";
  const cho = useChoices();
  const badge: JSX.Element = (
    <Badge pill bg={b}>
      {n +
        (props.obj === "prec" && props.link
          ? getChoice(cho, props.obj, props.link, false)
            ? " ✔"
            : " ✖"
          : "")}
    </Badge>
  );
  if (props.link)
    return <Link to={`/db/${props.obj}/${props.link}`}>{badge}</Link>;
  else return badge;
}

export function VulnPrecsBadge(props: { id: string }): JSX.Element {
  const cho = useChoices();
  const { applied, unapplied } = getVulnPrecsCount(cho, props.id);
  return (
    <Link to={`/db/vuln/${props.id}`}>
      <Badge pill bg={badgeMeta["prec"].b}>
        {applied} ✔ | {unapplied} ✖
      </Badge>
    </Link>
  );
}

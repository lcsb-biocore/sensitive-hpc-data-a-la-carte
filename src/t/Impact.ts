export type Impact = {
  label: string;
  description: string;
  estimation: string;
  estimate: number;
};

export type Impacts = Record<string, Impact>;

export type Vuln = {
  label: string;
  description: string;
  threats: string[];
  impacts: string[];
  precs: string[];
  amplifies?: string[];
  estimation: string;
  estimate: number;
};

export type Vulns = Record<string, Vuln>;
